import { mutations, actions, getters } from "@/store/modules/product";
import ProductService from "@/services/ProductService";

jest.mock("@/services/ProductService");

describe("Product Module", () => {
  describe("Mutations", () => {
    describe("FETCH_PRODUCTS", () => {
      it("should trigger loading state", () => {
        const stateBefore = {
          products: {
            loading: false,
          },
        };

        const stateAfter = {
          products: {
            loading: true,
          },
        };

        mutations.FETCH_PRODUCTS(stateBefore);
        expect(stateBefore).toStrictEqual(stateAfter);
      });
    });
    describe("FETCH_PRODUCTS_SUCCESS", () => {
      it("should normalize product data and store in state", () => {
        const stateBefore = {
          products: {
            byIds: {},
            allIds: [],
            currentIds: {},
            loading: true,
          },
        };

        const stateAfter = {
          products: {
            byIds: {
              1: {
                id: 1,
                name: "Product 1",
                sku: {
                  price: 100,
                },
              },
            },
            allIds: [1],
            currentIds: [1],
            loading: false,
          },
          meta: undefined,
        };

        const payload = {
          products: [{ id: 1, name: "Product 1", sku: { price: 100 } }],
          meta: undefined,
        };

        mutations.FETCH_PRODUCTS_SUCCESS(stateBefore, payload);
        expect(stateBefore).toStrictEqual(stateAfter);
      });

      it("should append byIds,allIds and update currentIds when FETCH_PRODUCT_SUCCESS again", () => {
        const stateBefore = {
          products: {
            byIds: {
              1: {
                id: 1,
                name: "Product 1",
                sku: {
                  price: 100,
                },
              },
            },
            allIds: [1, 2, 3],
            currentIds: [1, 2, 3],
            loading: false,
          },
          meta: undefined,
        };

        const stateAfter = {
          products: {
            byIds: {
              1: {
                id: 1,
                name: "Product 1",
                sku: {
                  price: 100,
                },
              },
              2: {
                id: 2,
                name: "Product 2",
                sku: {
                  price: 200,
                },
              },
              3: {
                id: 3,
                name: "Product 3",
                sku: {
                  price: 300,
                },
              },
            },
            allIds: [1, 2, 3],
            currentIds: [2, 3],
            loading: false,
          },
          meta: undefined,
        };

        const payload = {
          products: [
            { id: 2, name: "Product 2", sku: { price: 200 } },
            { id: 3, name: "Product 3", sku: { price: 300 } },
          ],
          meta: undefined,
        };

        mutations.FETCH_PRODUCTS_SUCCESS(stateBefore, payload);
        expect(stateBefore).toStrictEqual(stateAfter);
      });
    });
  });

  describe("Actions", () => {
    describe("fetchProducts", () => {
      it("ProductService.getProducts should have call with params", async () => {
        const rootState = {
          filterModule: {
            filters: {
              categoryIds: [],
              brandIds: [],
              page: 1,
            },
          },
        };

        const response = {
          data: {
            data: [],
            meta: {},
          },
        };

        ProductService.getProducts.mockReturnValue(response);

        await actions.fetchProducts({
          commit: jest.fn(),
          rootState,
        });

        expect(ProductService.getProducts).toHaveBeenCalledWith("", "", 1);
      });

      it("ProductService.getProducts should have call with specific params", async () => {
        const rootState = {
          filterModule: {
            filters: {
              categoryIds: [1, 2, 3],
              brandIds: [17, 18, 19],
              page: 4,
            },
          },
        };

        const response = {
          data: {
            data: [],
            meta: {},
          },
        };

        ProductService.getProducts.mockReturnValue(response);

        await actions.fetchProducts({
          commit: jest.fn(),
          rootState,
        });

        expect(ProductService.getProducts).toHaveBeenCalledWith(
          "1,2,3",
          "17,18,19",
          4
        );
      });

      it("should fetch products and pass payload when success", async () => {
        const response = {
          data: {
            data: [],
            meta: {},
          },
        };
        ProductService.getProducts.mockReturnValue(response);

        const rootState = {
          filterModule: {
            filters: {
              categoryIds: [],
              brandIds: [],
              page: 1,
            },
          },
        };
        const commit = jest.fn();

        await actions.fetchProducts({ commit, rootState });

        const payload = {
          products: response.data.data,
          meta: {},
        };

        expect(commit).toHaveBeenCalledWith("FETCH_PRODUCTS");
        expect(commit).toHaveBeenCalledWith("FETCH_PRODUCTS_SUCCESS", payload);
      });
    });
  });

  describe("Getters", () => {
    let state;
    beforeEach(() => {
      state = {
        products: {
          byIds: {
            1: {
              id: 1,
              name: "PRODUCT 1",
              sku: {
                price: 100,
              },
            },
            2: {
              id: 2,
              name: "PRODUCT 2",
              sku: {
                price: 200,
              },
            },
            3: {
              id: 3,
              name: "PRODUCT 3",
              sku: {
                price: 300,
              },
            },
            4: {
              id: 4,
              name: "PRODUCT 4",
              sku: {
                price: 400,
              },
            },
            5: {
              id: 5,
              name: "PRODUCT 5",
              sku: {
                price: 500,
              },
            },
          },
          allIds: [1, 2, 3, 4, 5],
          currentIds: [1, 3, 5],
        },
      };
    });

    it("getProducts", () => {
      const actual = getters.getProducts(state);
      expect(actual).toEqual([
        state.products.byIds[1],
        state.products.byIds[3],
        state.products.byIds[5],
      ]);
    });

    it("getAllProducts", () => {
      const actual = getters.getAllProducts(state);
      expect(actual).toEqual([
        state.products.byIds[1],
        state.products.byIds[2],
        state.products.byIds[3],
        state.products.byIds[4],
        state.products.byIds[5],
      ]);
    });
  });
});
