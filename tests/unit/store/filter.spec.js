import { mutations } from "@/store/modules/filter";
import ProductService from "@/services/ProductService";
import { actions, getters } from "../../../src/store/modules/filter";

jest.mock("@/services/ProductService");

describe("Filter Module", () => {
  describe("Mutation", () => {
    it("INIT_FILTER_DATA", () => {
      const stateBefore = {
        brands: {
          byIds: {},
          allIds: [],
        },
        categories: {
          byIds: {},
          allIds: [],
        },
        filters: {
          brandIds: [],
          categoryIds: [],
          page: 1,
        },
      };

      const payload = {
        brands: [
          {
            id: 1,
            name: "Brand 1",
          },
          {
            id: 2,
            name: "Brand 2",
          },
        ],
        categories: [
          {
            id: 1001,
            name: "Parent Category 1",
            categorie_parent_id: 0,
            childs: [
              {
                id: 90001,
                categorie_parent_id: 1001,
                name: "Child Category 1",
              },
              {
                id: 90002,
                categorie_parent_id: 1001,
                name: "Child Category 2",
              },
            ],
          },
          {
            id: 1002,
            name: "Parent Category 2",
            categorie_parent_id: 0,
            childs: [
              {
                id: 90003,
                categorie_parent_id: 1002,
                name: "Child Category 3",
              },
            ],
          },
          {
            id: 1003,
            name: "Parent Category 3",
            categorie_parent_id: 0,
            childs: [
              {
                id: 90004,
                categorie_parent_id: 1003,
                name: "Child Category 4",
              },
              {
                id: 90005,
                categorie_parent_id: 1003,
                name: "Child Category 5",
              },
            ],
          },
          {
            id: 90001,
            categorie_parent_id: 1001,
            name: "Child Category 1",
          },
          {
            id: 90002,
            categorie_parent_id: 1001,
            name: "Child Category 2",
          },
          {
            id: 90003,
            categorie_parent_id: 1002,
            name: "Child Category 3",
          },
          {
            id: 90004,
            categorie_parent_id: 1003,
            name: "Child Category 4",
          },
          {
            id: 90005,
            categorie_parent_id: 1003,
            name: "Child Category 5",
          },
        ],
        filters: {
          brandIds: [1, 2, 3],
          categoryIds: [4, 5, 6],
          page: 3,
        },
      };

      const stateAfter = {
        brands: {
          byIds: {
            1: {
              id: 1,
              name: "Brand 1",
            },
            2: {
              id: 2,
              name: "Brand 2",
            },
          },
          allIds: [1, 2],
        },
        categories: {
          byIds: {
            1001: {
              id: 1001,
              name: "Parent Category 1",
              categorie_parent_id: 0,
              childs: [90001, 90002],
            },
            1002: {
              id: 1002,
              name: "Parent Category 2",
              categorie_parent_id: 0,
              childs: [90003],
            },
            1003: {
              id: 1003,
              name: "Parent Category 3",
              categorie_parent_id: 0,
              childs: [90004, 90005],
            },
            90001: {
              id: 90001,
              categorie_parent_id: 1001,
              name: "Child Category 1",
            },
            90002: {
              id: 90002,
              categorie_parent_id: 1001,
              name: "Child Category 2",
            },
            90003: {
              id: 90003,
              categorie_parent_id: 1002,
              name: "Child Category 3",
            },
            90004: {
              id: 90004,
              categorie_parent_id: 1003,
              name: "Child Category 4",
            },
            90005: {
              id: 90005,
              categorie_parent_id: 1003,
              name: "Child Category 5",
            },
          },
          allIds: [1001, 1002, 1003, 90001, 90002, 90003, 90004, 90005],
        },
        filters: {
          brandIds: [1, 2, 3],
          categoryIds: [4, 5, 6],
          page: 3,
        },
      };

      mutations.INIT_FILTER_DATA(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });
    it("CHANGE_PAGE", () => {
      const stateBefore = {
        filters: {
          page: 1,
        },
      };
      const payload = 2;
      const stateAfter = {
        filters: {
          page: 2,
        },
      };

      mutations.CHANGE_PAGE(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });
    it("SET_BRAND_FILTERS", () => {
      const stateBefore = {
        filters: {
          brandIds: [],
        },
      };

      const payload = [1, 2, 3];

      const stateAfter = {
        filters: {
          brandIds: [1, 2, 3],
        },
      };

      mutations.SET_BRAND_FILTERS(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });
    it("SET_CATEGORY_FILTERS", () => {
      const stateBefore = {
        filters: {
          categoryIds: [],
        },
      };

      const payload = [100, 200, 300];

      const stateAfter = {
        filters: {
          categoryIds: [100, 200, 300],
        },
      };

      mutations.SET_CATEGORY_FILTERS(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });

    it("REMOVE_CATEGORY_FILTER", () => {
      const stateBefore = {
        filters: {
          categoryIds: [1, 2, 3],
        },
      };

      const payload = 2;

      const stateAfter = {
        filters: {
          categoryIds: [1, 3],
        },
      };

      mutations.REMOVE_CATEGORY_FILTER(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });

    it("REMOVE_BRAND_FILTER", () => {
      const stateBefore = {
        filters: {
          brandIds: [100, 200, 300],
        },
      };

      const payload = 300;

      const stateAfter = {
        filters: {
          brandIds: [100, 200],
        },
      };

      mutations.REMOVE_BRAND_FILTER(stateBefore, payload);
      expect(stateBefore).toStrictEqual(stateAfter);
    });

    it("CLEAR_FILTER", () => {
      const stateBefore = {
        filters: {
          categoryIds: [1, 3],
          brandIds: [100, 200, 300],
          page: 3,
        },
      };

      const stateAfter = {
        filters: {
          categoryIds: [],
          brandIds: [],
          page: 1,
        },
      };

      mutations.CLEAR_FILTER(stateBefore);
      expect(stateBefore).toStrictEqual(stateAfter);
    });
  });

  describe("Actions", () => {
    it("initFilterData", async () => {
      const commit = jest.fn();
      const dispatch = jest.fn();
      const response = {
        data: {
          categories: [],
          brands: [],
        },
      };

      const params = { categoryId: "", brandId: "", page: 1 };
      const payload = {
        brands: [],
        categories: [],
        filters: {
          brandIds: [],
          categoryIds: [],
          page: 1,
        },
      };

      ProductService.getAllBrands.mockReturnValue(response);
      await actions.initFilterData({ commit, dispatch }, params);

      expect(commit).toHaveBeenCalledWith("INIT_FILTER_DATA", payload);
      expect(dispatch).toHaveBeenCalledWith(
        "productModule/fetchProducts",
        null,
        { root: true }
      );
    });
    it("changePage", async () => {
      const commit = jest.fn();
      const dispatch = jest.fn();
      const params = 3;

      await actions.changePage({ commit, dispatch }, params);
      expect(commit).toHaveBeenCalledWith("CHANGE_PAGE", params);
      expect(dispatch).toHaveBeenCalledWith(
        "productModule/fetchProducts",
        null,
        {
          root: true,
        }
      );
    });

    it("clearFilter", async () => {
      const commit = jest.fn();
      const dispatch = jest.fn();
      await actions.clearFilter({ commit, dispatch });
      expect(commit).toHaveBeenCalledWith("CLEAR_FILTER");
      expect(dispatch).toHaveBeenCalledWith("changePage", 1);
    });

    describe("closeTag", () => {
      it("should commit REMOVE_CATEGORY_FILTER when category_parent_id is present", async () => {
        const commit = jest.fn();
        const dispatch = jest.fn();
        const tag = {
          id: 1,
          category_parent_id: 0,
        };
        await actions.closeTag({ commit, dispatch }, tag);
        expect(commit).toHaveBeenCalledWith("REMOVE_CATEGORY_FILTER", tag.id);
        expect(dispatch).toHaveBeenCalledWith("changePage", 1);
      });

      it("should commit REMOVE_BRANDS_FILTER when category_parent_id is present", async () => {
        const commit = jest.fn();
        const dispatch = jest.fn();
        const tag = {
          id: 1,
          brand_parent_id: 0,
        };
        await actions.closeTag({ commit, dispatch }, tag);
        expect(commit).toHaveBeenCalledWith("REMOVE_BRAND_FILTER", tag.id);
        expect(dispatch).toHaveBeenCalledWith("changePage", 1);
      });
    });

    it("setBrandFilters", async () => {
      const commit = jest.fn();
      const dispatch = jest.fn();
      const filters = [1, 2, 3];
      await actions.setBrandFilters({ commit, dispatch }, filters);
      expect(commit).toHaveBeenCalledWith("SET_BRAND_FILTERS", filters);
      expect(dispatch).toHaveBeenCalledWith("changePage", 1);
    });

    it("setCategoryFilters", async () => {
      const commit = jest.fn();
      const dispatch = jest.fn();
      const filters = [1, 2, 3];
      await actions.setCategoryFilters({ commit, dispatch }, filters);
      expect(commit).toHaveBeenCalledWith("SET_CATEGORY_FILTERS", filters);
      expect(dispatch).toHaveBeenCalledWith("changePage", 1);
    });
  });

  describe("GETTERS", () => {
    describe("getBrands", () => {
      const state = {
        brands: {
          byIds: {
            1: {
              id: 1,
              name: "Brand 1",
            },
            2: {
              id: 2,
              name: "Brand 2",
            },
            3: {
              id: 3,
              name: "Brand 3",
            },
            4: {
              id: 4,
              name: "Brand 4",
            },
          },
          allIds: [1, 2, 3, 4],
        },
      };

      const data = [
        state.brands.byIds[1],
        state.brands.byIds[2],
        state.brands.byIds[3],
        state.brands.byIds[4],
      ];

      const actual = getters.getBrands(state);
      expect(actual).toStrictEqual(data);
    });
    it("getCategories", () => {
      const state = {
        categories: {
          byIds: {
            1001: {
              id: 1001,
              name: "Parent Category 1",
              categorie_parent_id: 0,
              childs: [90001, 90002],
            },
            1002: {
              id: 1002,
              name: "Parent Category 2",
              categorie_parent_id: 0,
              childs: [90003],
            },
            1003: {
              id: 1003,
              name: "Parent Category 3",
              categorie_parent_id: 0,
              childs: [90004, 90005],
            },
            90001: {
              id: 90001,
              categorie_parent_id: 1001,
              name: "Child Category 1",
            },
            90002: {
              id: 90002,
              categorie_parent_id: 1001,
              name: "Child Category 2",
            },
            90003: {
              id: 90003,
              categorie_parent_id: 1002,
              name: "Child Category 3",
            },
            90004: {
              id: 90004,
              categorie_parent_id: 1003,
              name: "Child Category 4",
            },
            90005: {
              id: 90005,
              categorie_parent_id: 1003,
              name: "Child Category 5",
            },
          },
          allIds: [1001, 1002, 1003, 90001, 90002, 90003, 90004, 90005],
        },
      };

      const result = [
        {
          id: 1001,
          name: "Parent Category 1",
          categorie_parent_id: 0,
          childs: [
            { id: 90001, categorie_parent_id: 1001, name: "Child Category 1" },
            { id: 90002, categorie_parent_id: 1001, name: "Child Category 2" },
          ],
        },
        {
          id: 1002,
          name: "Parent Category 2",
          categorie_parent_id: 0,
          childs: [
            { id: 90003, categorie_parent_id: 1002, name: "Child Category 3" },
          ],
        },
        {
          id: 1003,
          name: "Parent Category 3",
          categorie_parent_id: 0,
          childs: [
            { id: 90004, categorie_parent_id: 1003, name: "Child Category 4" },
            { id: 90005, categorie_parent_id: 1003, name: "Child Category 5" },
          ],
        },
        { id: 90001, categorie_parent_id: 1001, name: "Child Category 1" },
        { id: 90002, categorie_parent_id: 1001, name: "Child Category 2" },
        { id: 90003, categorie_parent_id: 1002, name: "Child Category 3" },
        { id: 90004, categorie_parent_id: 1003, name: "Child Category 4" },
        { id: 90005, categorie_parent_id: 1003, name: "Child Category 5" },
      ];

      const actual = getters.getCategories(state);
      expect(actual).toStrictEqual(result);
    });

    it("getFilterTags", () => {
      const state = {
        brands: {
          byIds: {
            1: {
              id: 1,
              name: "Brand 1",
            },
            2: {
              id: 2,
              name: "Brand 2",
            },
            3: {
              id: 3,
              name: "Brand 3",
            },
            4: {
              id: 4,
              name: "Brand 4",
            },
          },
          allIds: [1, 2, 3, 4],
        },
        categories: {
          byIds: {
            1001: {
              id: 1001,
              name: "Parent Category 1",
              categorie_parent_id: 0,
              childs: [90001, 90002],
            },
            1002: {
              id: 1002,
              name: "Parent Category 2",
              categorie_parent_id: 0,
              childs: [90003],
            },
            1003: {
              id: 1003,
              name: "Parent Category 3",
              categorie_parent_id: 0,
              childs: [90004, 90005],
            },
            90001: {
              id: 90001,
              categorie_parent_id: 1001,
              name: "Child Category 1",
            },
            90002: {
              id: 90002,
              categorie_parent_id: 1001,
              name: "Child Category 2",
            },
            90003: {
              id: 90003,
              categorie_parent_id: 1002,
              name: "Child Category 3",
            },
            90004: {
              id: 90004,
              categorie_parent_id: 1003,
              name: "Child Category 4",
            },
            90005: {
              id: 90005,
              categorie_parent_id: 1003,
              name: "Child Category 5",
            },
          },
          allIds: [1001, 1002, 1003, 90001, 90002, 90003, 90004, 90005],
        },
        filters: {
          brandIds: [1, 2, 3],
          categoryIds: [1001, 1002, 1003],
        },
      };
      const result = [
        state.categories.byIds[1001],
        state.categories.byIds[1002],
        state.categories.byIds[1003],
        state.brands.byIds[1],
        state.brands.byIds[2],
        state.brands.byIds[3],
      ];

      const actual = getters.getFilterTags(state);
      expect(actual).toStrictEqual(result);
    });
  });
});
