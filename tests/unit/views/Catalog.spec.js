import { shallowMount } from "@vue/test-utils";
import Catalog from "@/views/Catalog";

describe("Catalog", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(Catalog, {
        mocks: {
          $store: {
            dispatch: jest.fn(),
          },
          $route: {
            query: {
              brand_id: "",
              category_id: "",
              page: 1,
            },
          },
        },
      });
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe("Actions", () => {
    it("should call filterModule/initFilterData", () => {
      const dispatch = jest.fn();
      shallowMount(Catalog, {
        mocks: {
          $store: {
            dispatch,
          },
          $route: {
            query: {
              brand_id: "",
              category_id: "",
              page: 1,
            },
          },
        },
      });
      expect(dispatch).toHaveBeenCalledWith("filterModule/initFilterData", {
        brandId: "",
        categoryId: "",
        page: 1,
      });
    });
  });
});
