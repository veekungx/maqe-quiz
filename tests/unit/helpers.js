export const productFactory = () => {
  return {
    id: Math.floor(Math.random() * 10000000),
    name: "Product Name",
    sku: {
      price: Math.random().toFixed(2) * 1000,
    },
  };
};
