import Vuex from "vuex";
import { createLocalVue, shallowMount } from "@vue/test-utils";
import FilterTag from "@/components/FilterTag";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    filterModule: {
      namespaced: true,
      getters: {
        getFilterTags() {
          return [
            {
              id: 1,
              name: "Tag 1",
            },
            {
              id: 2,
              name: "Tag 2",
            },
            {
              id: 3,
              name: "Tag 3",
            },
          ];
        },
      },
      productModule: {
        namespaced: true,
      },
    },
  },
});
store.dispatch = jest.fn();

describe("FilterTag", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(FilterTag, { store, localVue });
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe("Store", () => {
    it("should render 3 of Tag", () => {
      const wrapper = shallowMount(FilterTag, { store, localVue });
      const actual = wrapper.findAllComponents({ name: "v-chip" });
      expect(actual).toHaveLength(3);
    });
  });
  describe("Dispatch", () => {
    it("should displat filterModule/closeTag action when click on close", () => {
      const tag = { id: 1, name: "Tag 1" };
      const wrapper = shallowMount(FilterTag, { store, localVue });
      wrapper.findComponent({ name: "v-chip" }).vm.$emit("click:close", tag);
      expect(store.dispatch).toHaveBeenCalledWith("filterModule/closeTag", tag);
    });
  });
});
