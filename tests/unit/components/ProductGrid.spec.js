import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import ProductGrid from "@/components/ProductGrid";
import { productFactory } from "../helpers.js";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    productModule: {
      namespaced: true,
      getters: {
        getProducts() {
          return [productFactory(), productFactory(), productFactory()];
        },
      },
    },
  },
});

describe("ProductGrid", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(ProductGrid, { store, localVue });
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe("Store", () => {
    it("should render 3 of ProductCard", () => {
      const wrapper = shallowMount(ProductGrid, { store, localVue });
      const actual = wrapper.findAllComponents({ name: "product-card" });
      expect(actual).toHaveLength(3);
    });
  });
});
