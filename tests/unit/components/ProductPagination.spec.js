import Vuex from "vuex";
import { createLocalVue, shallowMount } from "@vue/test-utils";
import ProductPagination from "@/components/ProductPagination";

const localVue = createLocalVue();
localVue.use(Vuex);

const mockStore = {
  modules: {
    productModule: {
      namespaced: true,
      state: {
        meta: {
          currentPage: 1,
          lastPage: 10,
        },
      },
    },
    filterModule: {
      namespaced: true,
      state: {
        filters: {
          page: 1,
        },
      },
    },
  },
};

const createConfig = (overrides) => {
  const store = new Vuex.Store(mockStore);
  const mocks = {};
  const propsData = {};

  return {
    ...{ store, localVue, mocks, propsData },
    ...overrides,
  };
};

describe("ProductPagination", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(ProductPagination, createConfig());
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe("Store", () => {
    it("should passing value to pagination :value", () => {
      const wrapper = shallowMount(ProductPagination, createConfig());
      const actual = wrapper
        .find("[data-testid='pagination']")
        .attributes("value");
      expect(actual).toContain(1);
    });

    it("should passing value to pagination :length", () => {
      const wrapper = shallowMount(ProductPagination, createConfig());
      const actual = wrapper
        .find("[data-testid='pagination']")
        .attributes("length");
      expect(actual).toContain(10);
    });
  });
  describe("Dispatch", () => {
    it("should dispatch a filterModule/changePage when onChangePage is emitted", async () => {
      const store = new Vuex.Store(mockStore);
      store.dispatch = jest.fn();
      const wrapper = shallowMount(ProductPagination, createConfig({ store }));
      wrapper.findComponent({ name: "v-pagination" }).vm.$emit("input", 1);
      expect(store.dispatch).toHaveBeenCalledWith("filterModule/changePage", 1);
    });
  });
});
