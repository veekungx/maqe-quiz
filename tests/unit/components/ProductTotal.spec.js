import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import ProductTotal from "@/components/ProductTotal";

const localVue = createLocalVue();
localVue.use(Vuex);

const createConfig = (overrides) => {
  const store = new Vuex.Store({
    modules: {
      productModule: {
        namespaced: true,
        state: {
          meta: {
            total: 300,
          },
        },
      },
    },
  });
  const mocks = {};
  const propsData = {};

  return {
    ...{ store, localVue, mocks, propsData },
    ...overrides,
  };
};

describe("ProductTotal", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(ProductTotal, createConfig());
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe("Store", () => {
    it("should render value from state.productModule.state.meta.total", async () => {
      const wrapper = shallowMount(ProductTotal, createConfig());
      const actual = wrapper.find("[data-testid='total'").text();
      expect(actual).toContain(300);
    });
  });
});
