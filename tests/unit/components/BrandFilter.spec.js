import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BrandFilter from "@/components/BrandFilter";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    filterModule: {
      namespaced: true,
      state: {
        filters: {
          brandIds: {},
        },
      },
      getters: {
        getBrands() {
          return [];
        },
      },
    },
  },
});
store.dispatch = jest.fn();
{
  /* <script>
import { mapGetters } from "vuex";
export default {
  name: "brand-filter",
  computed: {
    brandFilters: {
      get() {
        return this.$store.state.filterModule.filters.brandIds;
      }
    },
    ...mapGetters("filterModule", ["getBrands"])
  },
  methods: {
    updateBrandFilters(value) {
      this.$store.dispatch("filterModule/setBrandFilters", value);
    }
  }
};
</script> */
}

describe("BrandFilter", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(BrandFilter, { localVue, store });
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe("Action", () => {
    it("should dispatch filterModule/setBrandFilters", () => {
      const wrapper = shallowMount(BrandFilter, { localVue, store });
      wrapper
        .findComponent({ name: "v-list-item-group" })
        .vm.$emit("change", [1]);

      expect(
        store.dispatch
      ).toHaveBeenCalledWith("filterModule/setBrandFilters", [1]);
    });
  });
});
