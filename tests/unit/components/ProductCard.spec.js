import { shallowMount } from "@vue/test-utils";

import ProductCard from "@/components/ProductCard.vue";

const createConfig = (overrides) => {
  const propsData = {
    product: {
      name: "BOSNY VIT",
      sku: {
        price: "180.00",
      },
    },
  };
  return {
    ...{ mocks: {}, propsData },
    ...overrides,
  };
};

describe("ProductCard.vue", () => {
  describe("Snapshot", () => {
    it.only("should match snapshot", () => {
      const wrapper = shallowMount(ProductCard, createConfig());
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe("Props", () => {
    it("should render props.product.name", () => {
      const wrapper = shallowMount(ProductCard, createConfig());
      const actual = wrapper.find('[data-testid="product-name"]').text();
      expect(actual).toBe("BOSNY VIT");
    });
    it("should render props.product.sku.price", () => {
      const wrapper = shallowMount(ProductCard, createConfig());
      const actual = wrapper.find('[data-testid="product-price"]').text();
      expect(actual).toContain("180.00");
    });
  });

  describe("Features", () => {
    it("should have '฿' sign before price", () => {
      const wrapper = shallowMount(ProductCard, createConfig());
      const actual = wrapper
        .find('[data-testid="product-price"')
        .text()
        .split(" ")[0];

      expect(actual).toBe("฿");
    });

    it("should highlight price", () => {
      const wrapper = shallowMount(ProductCard, createConfig());
      const actual = wrapper.find('[data-testid="product-price"').classes();
      expect(actual).toContain("blue--text");
    });
  });
});
