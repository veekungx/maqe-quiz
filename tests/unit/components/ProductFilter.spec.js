import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import ProductFilter from "@/components/ProductFilter";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    filterModule: {
      namespaced: true,
    },
  },
});
store.dispatch = jest.fn();

describe("ProductFilter", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(ProductFilter);
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe("Components", () => {
    it("should have CategoryFilter", () => {
      const wrapper = shallowMount(ProductFilter);
      const actual = wrapper
        .findComponent({ name: "category-filter" })
        .exists();
      expect(actual).toBe(true);
    });
    it("should have BrandFilter", () => {
      const wrapper = shallowMount(ProductFilter);
      const actual = wrapper.findComponent({ name: "brand-filter" }).exists();
      expect(actual).toBe(true);
    });
  });

  describe("Actions", () => {
    it("should call filterModule/clearFilter when button click", () => {
      const dispatch = jest.fn();
      const wrapper = shallowMount(ProductFilter, {
        mocks: { $store: { dispatch } },
      });

      wrapper.findComponent({ name: "v-btn" }).vm.$emit("click");
      expect(dispatch).toHaveBeenCalledWith("filterModule/clearFilter");
    });
  });
});
