import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import CategoryFilter from "@/components/CategoryFilter";

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    filterModule: {
      namespaced: true,
      state: {
        filters: {
          brandIds: {},
        },
      },
      getters: {
        getCategories() {
          return [];
        },
      },
    },
  },
});
store.dispatch = jest.fn();

describe("CategoryFilter", () => {
  describe("Snapshot", () => {
    it("should match snapshot", () => {
      const wrapper = shallowMount(CategoryFilter, { store, localVue });
      expect(wrapper).toMatchSnapshot();
    });
  });
});
