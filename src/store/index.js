import Vue from "vue";
import Vuex from "vuex";
import * as productModule from "@/store/modules/product";
import * as filterModule from "@/store/modules/filter";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    productModule,
    filterModule,
  },
});
