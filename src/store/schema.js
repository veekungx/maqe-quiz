import { schema } from "normalizr";

export const categorySchema = new schema.Entity("categories");
export const categoryListSchema = new schema.Array(categorySchema);
categorySchema.define({ childs: categoryListSchema });
export const brandSchema = new schema.Entity("brands");
export const brandListSchema = new schema.Array(brandSchema);
export const productSchema = new schema.Entity("products");
productSchema.define({ brand: brandSchema });
export const productListSchema = new schema.Array(productSchema);
