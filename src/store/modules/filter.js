import { normalize, denormalize } from "normalizr";
import {
  brandListSchema,
  categorySchema,
  categoryListSchema,
} from "@/store/schema";
import ProductService from "@/services/ProductService";

export const namespaced = true;
export const state = {
  brands: {
    byId: {},
    allIds: [],
  },
  categories: {
    byId: {},
    allIds: [],
  },
  filters: {
    brandIds: [],
    categoryIds: [],
    page: 1,
  },
};

export const mutations = {
  INIT_FILTER_DATA(state, payload) {
    const normalizeBrands = normalize(payload.brands, brandListSchema);
    const normalizeCategories = normalize(
      payload.categories,
      categoryListSchema
    );
    state.brands = {
      byIds: { ...normalizeBrands.entities.brands },
      allIds: normalizeBrands.result,
    };
    state.categories = {
      byIds: { ...normalizeCategories.entities.categories },
      allIds: normalizeCategories.result,
    };
    state.filters = {
      brandIds: payload.filters.brandIds,
      categoryIds: payload.filters.categoryIds,
      page: payload.filters.page,
    };
  },
  CHANGE_PAGE(state, payload) {
    state.filters.page = payload;
  },
  SET_BRAND_FILTERS(state, payload) {
    state.filters.brandIds = payload;
  },
  SET_CATEGORY_FILTERS(state, payload) {
    state.filters.categoryIds = payload;
  },

  REMOVE_CATEGORY_FILTER(state, tagId) {
    state.filters.categoryIds = state.filters.categoryIds.filter(
      (categoryId) => categoryId !== tagId
    );
  },

  REMOVE_BRAND_FILTER(state, tagId) {
    state.filters.brandIds = state.filters.brandIds.filter(
      (brandId) => brandId !== tagId
    );
  },
  CLEAR_FILTER(state) {
    state.filters = {
      categoryIds: [],
      brandIds: [],
      page: 1,
    };
  },
};
export const actions = {
  async initFilterData(
    { commit, dispatch },
    { categoryId = "", brandId = "", page = 1 }
  ) {
    if (isNaN(page)) page = 1;
    const { data } = await ProductService.getAllBrands();
    const brandIds = brandId
      .split(",")
      .filter((item) => item !== "")
      .map((item) => +item);

    const categoryIds = categoryId
      .split(",")
      .filter((item) => item !== "")
      .map((item) => +item);

    const payload = {
      brands: data.brands,
      categories: data.categories,
      filters: {
        brandIds,
        categoryIds,
        page: parseInt(page),
      },
    };

    commit("INIT_FILTER_DATA", payload);
    await dispatch("productModule/fetchProducts", null, { root: true });
  },
  async changePage({ commit, dispatch }, page) {
    commit("CHANGE_PAGE", parseInt(page));
    await dispatch("productModule/fetchProducts", null, { root: true });
  },
  async clearFilter({ commit, dispatch }) {
    commit("CLEAR_FILTER");
    await dispatch("changePage", 1);
  },
  async closeTag({ commit, dispatch }, tag) {
    if ("category_parent_id" in tag) {
      commit("REMOVE_CATEGORY_FILTER", tag.id);
    } else if ("brand_parent_id" in tag) {
      commit("REMOVE_BRAND_FILTER", tag.id);
    }

    await dispatch("changePage", 1);
  },
  async setBrandFilters({ commit, dispatch }, brandFilters = []) {
    commit("SET_BRAND_FILTERS", brandFilters);
    await dispatch("changePage", 1);
  },
  async setCategoryFilters({ commit, dispatch }, categoryFilters = []) {
    commit("SET_CATEGORY_FILTERS", categoryFilters);
    await dispatch("changePage", 1);
  },
};
export const getters = {
  getBrands(state) {
    return state.brands.allIds.map((id) => state.brands.byIds[id]);
  },
  getCategories(state) {
    const schema = { categories: [categorySchema] };
    const entities = { categories: state.categories.byIds };
    const data = { categories: state.categories.allIds };
    const { categories } = denormalize(data, schema, entities);
    return categories;
  },
  getFilterTags(state) {
    const categories = state.filters.categoryIds.map(
      (categoryId) => state.categories.byIds[categoryId]
    );
    const brands = state.filters.brandIds.map(
      (brandId) => state.brands.byIds[brandId]
    );
    return [...categories, ...brands];
  },
};
