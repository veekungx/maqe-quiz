import { normalize } from "normalizr";
import router from "@/router";
import ProductService from "@/services/ProductService";
import { productListSchema } from "@/store/schema";

const go = (categoryFilters = [], brandFilters = [], page = 1) => {
  router
    .push({
      path: "/catalog",
      query: {
        brand_id: brandFilters.join(","),
        category_id: categoryFilters.join(","),
        page: page,
      },
    })
    .catch(() => {});
};

export const namespaced = true;
export const state = {
  products: {
    byIds: {},
    allIds: [],
    currentIds: [],
    loading: false,
  },
  meta: {
    total: 0,
    to: null,
    from: null,
    lastPage: null,
    perPage: null,
    currentPage: 1,
  },
};

export const mutations = {
  FETCH_PRODUCTS(state) {
    state.products.loading = true;
  },
  FETCH_PRODUCTS_SUCCESS(state, payload) {
    const { entities, result } = normalize(payload.products, productListSchema);
    state.products.byIds = {
      ...state.products.byIds,
      ...entities.products,
    };
    state.products.allIds = [...new Set([...state.products.allIds, ...result])];
    state.products.currentIds = result;
    state.products.loading = false;
    state.meta = payload.meta;
  },
};

export const actions = {
  async fetchProducts({ commit, rootState }) {
    commit("FETCH_PRODUCTS");

    const { data } = await ProductService.getProducts(
      rootState.filterModule.filters.categoryIds.join(","),
      rootState.filterModule.filters.brandIds.join(","),
      rootState.filterModule.filters.page
    );

    const payload = {
      products: data.data,
      meta: {
        currentPage: data.meta.current_page,
        from: data.meta.from,
        lastPage: data.meta.last_page,
        path: data.meta.path,
        perPage: data.meta.per_page,
        to: data.meta.to,
        total: data.meta.total,
      },
    };

    commit("FETCH_PRODUCTS_SUCCESS", payload);
    go(
      rootState.filterModule.filters.categoryIds,
      rootState.filterModule.filters.brandIds,
      rootState.filterModule.filters.page
    );
  },
};

export const getters = {
  getProducts(state) {
    return state.products.currentIds.map(
      (productId) => state.products.byIds[productId]
    );
  },
  getAllProducts(state) {
    return state.products.allIds.map(
      (productId) => state.products.byIds[productId]
    );
  },
};
