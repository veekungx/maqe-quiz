import Vue from "vue";
import VueRouter from "vue-router";
import Catalog from "@/views/Catalog.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Index",
    redirect: "/catalog",
  },
  {
    path: "/catalog",
    name: "Catalog",
    component: Catalog,
    props: (route) => ({
      brandId: route.query.brand_id,
      categoryId: route.query.category_id,
      page: parseInt(route.query.page),
    }),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
