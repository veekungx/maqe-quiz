import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://factools.qa.maqe.com",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  getProducts(categoryIds = "", brandIds = "", page = 1) {
    const url = `/catalog/filter?category_id=${categoryIds}&brand_id=${brandIds}&page=${page}`;
    return apiClient.get(url);
  },
  getAllBrands() {
    const url = "/catalog/allcatsbrands";
    return apiClient.get(url);
  },
};
